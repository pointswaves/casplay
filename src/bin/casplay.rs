use std::{
    collections::HashMap,
    ffi::OsStr,
    fs::metadata,
    io::{stdout, SeekFrom, Write},
    path::{Path, PathBuf},
};

use std::os::unix::fs::PermissionsExt;

use async_recursion::async_recursion;
use async_stream::stream;
use casplay::{
    build::bazel::remote::execution::v2::{
        capabilities_client::CapabilitiesClient,
        content_addressable_storage_client::ContentAddressableStorageClient, Digest, Directory,
        DirectoryNode, FileNode, FindMissingBlobsRequest, GetCapabilitiesRequest, GetTreeRequest,
        SymlinkNode,
    },
    google::bytestream::{byte_stream_client::ByteStreamClient, ReadRequest, WriteRequest},
    Uploader,
};

use clap::{Parser, Subcommand};
use lazy_static::__Deref;
use prost::Message;
use sha256::{digest_bytes, digest_file};
use tokio::fs::{read_dir, read_link, File};
use tokio::io::AsyncReadExt;
use tokio::io::AsyncSeekExt;
use tracing::info;

use async_std::{
    fs::File as AsFile,
    io::{copy, ReadExt, WriteExt},
    path::PathBuf as AsPathBuf,
    prelude::*,
    task::current,
};
use async_tar::{Archive, EntryType};

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about=None)]
struct Cli {
    #[clap(long)]
    endpoint: Option<String>,
    #[clap(long)]
    instance: Option<String>,

    #[clap(subcommand)]
    command: Command,
}

#[derive(Subcommand, Debug)]
enum Command {
    Caps,
    Fetch { name: String },
    Upload { name: PathBuf },
    Ls { name: String },
    Serve,
}

type Crapshoot = anyhow::Result<()>;

#[tokio::main]
async fn main() -> Crapshoot {
    real_main().await
}

struct Config {
    endpoint: String,
    instance_name: String,
}

async fn real_main() -> Crapshoot {
    let cli = Cli::parse();

    let endpoint = cli
        .endpoint
        .unwrap_or_else(|| "http://localhost:50052".into());

    let config = Config {
        endpoint,
        instance_name: cli.instance.unwrap_or_else(|| "".into()),
    };

    match cli.command {
        Command::Caps => capabilities(&config).await?,
        Command::Fetch { name } => fetch_blob(&config, &name).await?,
        Command::Upload { name } => upload_blob(&config, &name).await?,
        Command::Ls { name } => list_tree(&config, &name).await?,
        Command::Serve => serve(&config).await?,
    };

    Ok(())
}

async fn serve(config: &Config) -> Crapshoot {
    tracing_subscriber::fmt().init();
    info!("Starting server on port 5000");
    casplay::server::serve(([0, 0, 0, 0], 5000).into(), &config.instance_name).await?;
    Ok(())
}

async fn capabilities(config: &Config) -> Crapshoot {
    let request = GetCapabilitiesRequest {
        instance_name: config.instance_name.clone(),
    };

    let mut client = CapabilitiesClient::connect(config.endpoint.clone()).await?;

    let caps = client.get_capabilities(request).await?;

    let cachecaps = caps.into_inner().cache_capabilities.unwrap();

    println!("Output: {:#?}", cachecaps);

    Ok(())
}

async fn fetch_blob(config: &Config, name: &str) -> Crapshoot {
    let mut client = ByteStreamClient::connect(config.endpoint.clone()).await?;
    let request = ReadRequest {
        resource_name: format!("{}/blobs/{}", config.instance_name, name),
        read_offset: 0,
        read_limit: 0,
    };

    let mut reader = client.read(request).await?.into_inner();

    while let Some(msg) = reader.message().await? {
        stdout().write_all(&msg.data).unwrap();
    }

    Ok(())
}

async fn upload_blob(config: &Config, name: &Path) -> Crapshoot {
    if metadata(name)?.is_dir() {
        return upload_dir(config, name).await;
    }
    println!("file {:?}", name);
    let digest = if Some("tar") == name.extension().and_then(OsStr::to_str) {
        println!("its a tar");
        upload_tar(config, name).await?
    } else {
        println!("not tar");
        really_upload_blob(config, name).await?
    };

    println!("Blob uploaded as {}/{}", digest.hash, digest.size_bytes);
    Ok(())
}

/// This is fairly stupid
/// It assumes that everything is nicely listed and that
/// directories are created in order and that files are created
/// straight after the directory in which they live
async fn upload_tar(config: &Config, name: &Path) -> anyhow::Result<Digest> {
    let mut uploader = Uploader::new(
        config.endpoint.clone(),
        Some(config.instance_name.as_ref()),
        None,
    )
    .await?;

    let this_dir = Directory {
        files: vec![],
        directories: vec![],
        symlinks: vec![],
        node_properties: None,
    };
    let mut dir_map: HashMap<AsPathBuf, Directory> = HashMap::new();

    let mut fh = AsFile::open(name).await?;
    let mut ar = Archive::new(fh);
    let mut entries = ar.entries().unwrap();
    let mut count = 0;
    let mut current_dir: Vec<(AsPathBuf, Directory)> = vec![(AsPathBuf::from(""), this_dir)];

    while let Some(file) = entries.next().await {
        count += 1;
        if count > 100 {
            break;
        }
        let mut f = file.unwrap();
        println!("{}", f.path().unwrap().display());
        match f.header().entry_type() {
            EntryType::Regular => {
                println!("open");
                let mut tmp_file = AsFile::create("/tmp/mctemp").await?;
                println!("opened");
                copy(&mut f, &mut tmp_file).await.unwrap();
                //tmp_file.seek(SeekFrom::Start(0));
                tmp_file.flush().await?;
                drop(tmp_file);
                //let mut tmp_file = AsFile::open("/tmp/mctemp").await?;
                //println!("new file");
                //let digest = really_upload_blob_file(config, tmp_file).await?;
                let digest = really_upload_blob(config, &PathBuf::from(r"/tmp/mctemp")).await?;
                let executable = (f.header().mode().unwrap() & 0o111) != 0;

                let node = FileNode {
                    name: f
                        .path()
                        .unwrap()
                        .into_owned()
                        .file_name()
                        .unwrap()
                        .to_string_lossy()
                        .to_string(),
                    digest: Some(digest),
                    is_executable: executable,
                    node_properties: None,
                };

                while f.path().unwrap().parent().unwrap() != current_dir.last().unwrap().0{
                    if let Some((cur_buff, mut cur_dir)) = current_dir.pop(){
                        cur_dir.files.sort_by_key(|e| e.name.clone());
                        cur_dir.directories.sort_by_key(|e| e.name.clone());
                        cur_dir.symlinks.sort_by_key(|e| e.name.clone());
                        println!("push up {:?} {:?}", cur_buff, cur_dir);
                        let res = uploader.queue_message(cur_dir).await?;
                
                        let cur_digest = res.clone();
                
                        for ancestor in current_dir.iter_mut().rev() {
                            if ancestor.0 == cur_buff.parent().unwrap() {
                                let node = DirectoryNode {
                                    name: cur_buff.to_string_lossy().to_string(),
                                    digest: Some(cur_digest),
                                };
                                ancestor.1.directories.push(node);
                                break;
                            }
                        }
                    }

                }
                current_dir.last_mut().unwrap().1.files.push(node)
                //                dir_map
                //                    .get_mut(f.path().unwrap().parent().unwrap())
                //                    .unwrap()
                //                    .files
                //                    .push(node);
            }
            EntryType::Directory => {
                let mut new_dir = Directory {
                    files: vec![],
                    directories: vec![],
                    symlinks: vec![],
                    node_properties: None,
                };
                println!(
                    "{:?}, {:?}, {:?}",
                    f.path().unwrap().parent().unwrap(),
                    current_dir.last().unwrap().0,
                    f.path().unwrap().parent().unwrap() != current_dir.last().unwrap().0
                );
                while f.path().unwrap().parent().unwrap() != current_dir.last().unwrap().0 {
                    break;
                }

                println!("directory: {:?}", f.path().unwrap());
                let dir_path = f.path().unwrap().into_owned();
                current_dir.push((dir_path, new_dir));
            }
            _ => {}
        }
    }

    let mut this_result: Option<anyhow::Result<Digest>> = None;
    while let Some((cur_buff, mut cur_dir)) = current_dir.pop() {
        cur_dir.files.sort_by_key(|e| e.name.clone());
        cur_dir.directories.sort_by_key(|e| e.name.clone());
        cur_dir.symlinks.sort_by_key(|e| e.name.clone());
        println!("push up {:?} {:?}", cur_buff, cur_dir);
        let res = uploader.queue_message(cur_dir).await?;
        print!("res {:?}", res);

        let cur_digest = res.clone();
        this_result = Some(Ok(res));

        for ancestor in current_dir.iter_mut().rev() {
            if ancestor.0 == cur_buff.parent().unwrap() {
                let node = DirectoryNode {
                    name: cur_buff.to_string_lossy().to_string(),
                    digest: Some(cur_digest),
                };
                ancestor.1.directories.push(node);
                break;
            }
        }
    }
    let mut this_result = this_result.unwrap();
    uploader.flush().await?;
    this_result
}

async fn really_upload_blob_file(config: &Config, mut fh: AsFile) -> anyhow::Result<Digest> {
    let mut raw_bytes = vec![];
    // This will go badly if the file is large and the avaliable
    // ram is small but its what the other one does under the hood
    // so isnt any worce although it needs making not bad.
    fh.read(&mut raw_bytes).await?;
    println!("raw bypes {:?}", raw_bytes);
    let sha = digest_bytes(&raw_bytes[..]);
    let flen = raw_bytes.len();

    let base_request = WriteRequest {
        resource_name: format!(
            "{}/uploads/idontknowwhatgoeshere/blobs/{}/{}",
            config.instance_name, sha, flen
        ),
        write_offset: 0,
        finish_write: false,
        data: Vec::new(),
    };

    // This seems like a not auful idea but not sure how well it works.
    fh.seek(SeekFrom::Start(0)).await?;

    let mut client = ByteStreamClient::connect(config.endpoint.clone()).await?;

    let mut buffer = Vec::with_capacity(4096);

    let stream = stream! {
        let mut total_bytes = 0;
        if loop {
            buffer.resize(4096,0u8);
            let bytecount = match fh.read(&mut buffer[..]).await {
                Ok(n) => n,
                Err(_) => break false,
            };
            buffer.resize(bytecount,0);
            let mut request = base_request.clone();
            request.write_offset = total_bytes;
            request.data = std::mem::replace(&mut buffer, Vec::with_capacity(4096));
            yield request;
            total_bytes += bytecount as i64;
            if bytecount != 4096 {
                break true;
            } }{
                let mut request = base_request.clone();
                request.write_offset = total_bytes;
                request.finish_write = true;
                yield request;
            }

    };

    println!("About to call write!");
    let response = client.write(stream).await?;

    if response.get_ref().committed_size != flen as i64 {
        anyhow::bail!("Woah, committed size was wrong");
    }

    Ok(Digest {
        hash: sha,
        size_bytes: flen as i64,
    })
}

async fn really_upload_blob(config: &Config, name: &Path) -> anyhow::Result<Digest> {
    let sha = digest_file(name)?;
    let flen = metadata(name)?.len();

    // if has_blob(config, &sha, flen).await? {
    //     return Ok(Digest {
    //         hash: sha,
    //         size_bytes: flen as i64,
    //     });
    // }

    let base_request = WriteRequest {
        resource_name: format!(
            "{}/uploads/idontknowwhatgoeshere/blobs/{}/{}",
            config.instance_name, sha, flen
        ),
        write_offset: 0,
        finish_write: false,
        data: Vec::new(),
    };

    let mut client = ByteStreamClient::connect(config.endpoint.clone()).await?;

    let mut fh = File::open(name).await?;
    let mut buffer = Vec::with_capacity(4096);

    let stream = stream! {
        let mut total_bytes = 0;
        if loop {
            buffer.resize(4096,0u8);
            let bytecount = match fh.read(&mut buffer[..]).await {
                Ok(n) => n,
                Err(_) => break false,
            };
            buffer.resize(bytecount,0);
            let mut request = base_request.clone();
            request.write_offset = total_bytes;
            request.data = std::mem::replace(&mut buffer, Vec::with_capacity(4096));
            yield request;
            total_bytes += bytecount as i64;
            if bytecount != 4096 {
                break true;
            } }{
                let mut request = base_request.clone();
                request.write_offset = total_bytes;
                request.finish_write = true;
                yield request;
            }

    };

    println!("About to call write!");
    let response = client.write(stream).await?;

    if response.get_ref().committed_size != flen as i64 {
        anyhow::bail!("Woah, committed size was wrong");
    }

    Ok(Digest {
        hash: sha,
        size_bytes: flen as i64,
    })
}

#[allow(dead_code)]
async fn has_blob(config: &Config, sha: &str, flen: u64) -> anyhow::Result<bool> {
    let mut client = ContentAddressableStorageClient::connect(config.endpoint.clone()).await?;
    let request = FindMissingBlobsRequest {
        instance_name: config.instance_name.clone(),
        blob_digests: vec![Digest {
            hash: sha.to_string(),
            size_bytes: flen as i64,
        }],
    };
    let response = client.find_missing_blobs(request).await?.into_inner();
    Ok(response.missing_blob_digests.is_empty())
}

fn digest_from_str(input: &str) -> Digest {
    let parts: Vec<_> = input.split('/').collect();
    assert_eq!(parts.len(), 2);
    Digest {
        hash: parts[0].into(),
        size_bytes: parts[1].parse().unwrap(),
    }
}

async fn list_tree(config: &Config, name: &str) -> Crapshoot {
    let mut client = ContentAddressableStorageClient::connect(config.endpoint.clone()).await?;
    let root_digest = digest_from_str(name);
    let request = GetTreeRequest {
        instance_name: config.instance_name.clone(),
        root_digest: Some(root_digest.clone()),
        page_size: 100,
        page_token: Default::default(),
    };

    let mut response = client.get_tree(request).await?.into_inner();

    let mut namemap: HashMap<String, String> = HashMap::new();

    namemap.insert(
        format!("{}/{}", root_digest.hash, root_digest.size_bytes),
        "".into(),
    );

    while let Some(response) = response.message().await? {
        for dir in response.directories.iter() {
            let enc = dir.encode_to_vec();
            let sha = digest_bytes(&enc);
            let digest = format!("{}/{}", sha, enc.len());

            let this_name = namemap.get(&digest).unwrap().to_string();
            for file in &dir.files {
                if let Some(digest) = &file.digest {
                    println!(
                        "{}/{} is {} file {}/{}",
                        this_name,
                        file.name,
                        if file.is_executable {
                            "an executable"
                        } else {
                            "a"
                        },
                        digest.hash,
                        digest.size_bytes
                    );
                } else {
                    println!(
                        "{}/{} is {} file (No digest?)",
                        this_name,
                        file.name,
                        if file.is_executable {
                            "an executable"
                        } else {
                            "a"
                        }
                    );
                }
            }
            for dir in &dir.directories {
                if let Some(digest) = &dir.digest {
                    namemap.insert(
                        format!("{}/{}", digest.hash, digest.size_bytes),
                        format!("{}/{}", this_name, dir.name),
                    );
                }
            }
            for link in &dir.symlinks {
                println!("{}/{} -> {}", this_name, link.name, link.target);
            }
        }
    }

    Ok(())
}

async fn upload_dir(config: &Config, name: &Path) -> Crapshoot {
    let mut uploader = Uploader::new(
        config.endpoint.clone(),
        Some(config.instance_name.as_ref()),
        None,
    )
    .await?;
    let res = upload_dir_(&mut uploader, config, name, "").await?;
    uploader.flush().await?;
    println!(
        "Uploaded directory {}, digest is {}/{}",
        name.display(),
        res.hash,
        res.size_bytes
    );
    Ok(())
}

#[async_recursion]
async fn upload_dir_(
    uploader: &mut Uploader,
    config: &Config,
    dir: &Path,
    base_name: &str,
) -> anyhow::Result<Digest> {
    let mut dir_reader = read_dir(dir).await?;

    let mut this_dir = Directory {
        files: vec![],
        directories: vec![],
        symlinks: vec![],
        node_properties: None,
    };

    while let Some(entry) = dir_reader.next_entry().await? {
        // Do something with the entry
        let ftype = entry.file_type().await?;
        let name = entry.file_name().to_string_lossy().to_string();
        if ftype.is_dir() {
            let subname = format!("{}{}/", base_name, name);
            let digest =
                upload_dir_(uploader, config, &dir.join(entry.file_name()), &subname).await?;
            let node = DirectoryNode {
                name: name.clone(),
                digest: Some(digest),
            };
            this_dir.directories.push(node);
        }
        if ftype.is_file() {
            let digest = uploader.upload_file(&entry.path()).await?;
            let perms = entry.metadata().await?.permissions();
            let executable = (perms.mode() & 0o111) != 0;
            let node = FileNode {
                name: name.clone(),
                digest: Some(digest),
                is_executable: executable,
                node_properties: None,
            };
            this_dir.files.push(node);
        }
        if ftype.is_symlink() {
            let target = read_link(entry.path()).await?;
            let target = format!("{}", target.display());
            let node = SymlinkNode {
                name: name.clone(),
                target,
                node_properties: None,
            };
            this_dir.symlinks.push(node);
        }
    }

    this_dir.files.sort_by_key(|e| e.name.clone());
    this_dir.directories.sort_by_key(|e| e.name.clone());
    this_dir.symlinks.sort_by_key(|e| e.name.clone());

    Ok(uploader.queue_message(this_dir).await?)
}
